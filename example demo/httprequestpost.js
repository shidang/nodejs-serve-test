/* 
* @Author: sd
* @Date:   2016-10-31 14:51:51
* @Last Modified by:   sd
* @Last Modified time: 2016-10-31 15:00:32
*/

// 通过http.request 发送post请求
var http = require('http');
var querystring = require('querystring');

var contents = querystring.stringify({
    name: 'loveb',
    phone: '12345678',
    email: 'loveb@126.com'
});
var option = {
    host: 'www.byvoid.com',
    path: '/application/node/post.php',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': contents.length
    }
};
var req = http.request(option,function(res){
    res.setEncoding('utf-8');
    res.on('data',function(data){
        console.log(data);
    });
});
req.write(contents);
req.end(); 