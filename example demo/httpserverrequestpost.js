/* 
* @Author: sd
* @Date:   2016-10-31 14:38:20
* @Last Modified by:   sd
* @Last Modified time: 2016-10-31 14:49:43
*/

// 获取post请求内容 demo  生产环境使用这种方法效率低下
var http = require('http');
var queryString = require('querystring');// 请求体模块
var util = require('util'); //常用函数集合
http.createServer(function(req,res){
    var post = '';
    req.on('data',function(chunk){  //接收数据
        post += chunk;
    });
    req.on('end',function(){ //请求体数据传输完成后触发
        post = queryString.parse(post);
        res.end(util.inspect(post));
    });
}).listen(3000);