/* 
* @Author: sd
* @Date:   2016-10-31 11:17:11
* @Last Modified by:   sd
* @Last Modified time: 2016-10-31 11:37:39
*/

var http = require('http');
http.createServer(function(req,res){
    res.writeHead(200,{'Content-Type': 'text/html'});
    res.write('<h1>node.js</h1>');
    res.end('<p>hello nodeJs</p>');
}).listen(8888);
console.log("HTTP server is listening at port 8888");
