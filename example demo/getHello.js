/* 
* @Author: sd
* @Date:   2016-10-31 12:18:27
* @Last Modified by:   sd
* @Last Modified time: 2016-10-31 14:37:09
*/


// 获取hello对象 
var hello = require('./hello');
hello = new hello();
hello.setName('sean');
hello.sayHello();
