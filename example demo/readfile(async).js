/* 
* @Author: sd
* @Date:   2016-10-31 11:45:13
* @Last Modified by:   sd
* @Last Modified time: 2016-10-31 11:48:57
*/

// 异步读取文件
var rf =  require('fs');
rf.readFile('file.txt', 'utf-8', function(err, data){
    if(err){
        console.error(err);
    }else {
        console.log(data);
    }
});
console.log('end');