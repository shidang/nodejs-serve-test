/* 
* @Author: sd
* @Date:   2016-10-31 14:25:14
* @Last Modified by:   sd
* @Last Modified time: 2016-10-31 14:37:56
*/

// 获取get请求内容
var http = require('http');
var url = require('url') //解析get请求参数模块
var util = require('util');// 常用函数集合
http.createServer(function(req, res){
    res.writeHead(200,{'Content-Type': 'text/plain'});
    res.end(util.inspect(url.parse(req.url,true)));//util.inspect 转为字符串，url.parse解析为对象
}).listen(3000)

/*  
在url中输入http://127.0.0.1:3000/user?name=lovebhapp&phone=134221

获得返回结果
Url {
  protocol: null,
  slashes: null,
  auth: null,
  host: null,
  port: null,
  hostname: null,
  hash: null,
  search: '?name=lovebhapp&phone=134221',
  query: { name: 'lovebhapp', phone: '134221' },
  pathname: '/user',
  path: '/user?name=lovebhapp&phone=134221',
  href: '/user?name=lovebhapp&phone=134221' }
 */