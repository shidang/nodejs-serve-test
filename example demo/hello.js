/* 
* @Author: sd
* @Date:   2016-10-31 12:15:53
* @Last Modified by:   sd
* @Last Modified time: 2016-10-31 12:21:01
*/

// 获取单一对象

function hello (){
    var name;
    this.setName = function(thyName){
        name = thyName;
    };
    this.sayHello = function(){
        console.log('hello' + name);
    }
}
module.exports = hello;