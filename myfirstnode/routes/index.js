
var sql = require('./sqldata');
module.exports = function(app) {
    /*demo*/
    // app.get('/', function (req, res) {
    //     res.render('index', { title: 'Express html' });
    // });

    // app.all("/user/:name", function(req, res,next) {
    //     console.log("all methods captured"); 
    //     next();
    // });

    // app.get("/user/:name", function(req, res) {
    //     res.send("user: " + req.params.name);    
    // });


    // app.get("/list", function(req, res) {
    //     res.render('list',{
    //         title: 'List',
    //         items: [1991,2000,3000,4000]
    //     })   
    // });

    /*微博demo*/
    app.get('/',function(req,res){
        if(req.cookies.islogin){
            req.session.islogin = req.cookies.islogin; // 存储session
        }
        if(req.session.islogin){
            res.locals.islogin = req.session.islogin; // 存储全局
        }
        res.render('index', { title: '主页' ,test:res.locals.islogin});
    });
    app.get('/u/:user', function(req,res){

    });
    app.get('/post', function(req,res,next){
        res.render('post', { title: '发表' });
    });
    app.get('/reg', function(req,res,next){
        res.render('reg', { title: '注册' });
        
    });
    app.post('/reg', function(req,res,next){
    
        sql.insertNew(req,res,next);
    });
    app.get('/login', function(req,res){
        if(req.session.islogin){
            res.locals.islogin = req.session.islogin;
        }
        if(req.cookies.islogin){
            req.session.islogin = req.cookies.islogin;
        }
        res.render('login', { title: '登录',test:res.locals.islogin});

    });
    app.post('/login', function(req,res,next){
        sql.selectUser(req,res,next);
    });
    app.get('/logout', function(req,res,next){
       res.clearCookie('islogin');// clear cookie
       req.session.destroy(); // clear session
       res.redirect('/'); // 重定向
    });
    app.get('/del',function(req,res,next) {
        sql.deleteAll(req,res,next);
    })



};