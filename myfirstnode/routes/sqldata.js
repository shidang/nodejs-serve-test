/* 
* @Author: sd
* @Date:   2016-11-03 15:10:10
* @Last Modified by:   sd
* @Last Modified time: 2016-11-08 15:47:50
*/

/*数据逻辑层*/

 //调用MySQL模块
var pool = require('./sqlheader');

// 向前台返回JSON方法的简单封装
var jsonWrite = function(res, ret){
    if(typeof ret === 'undefined'){
        res.json({
            code:1,
            msg: '操作失败'
        })
    } else {
        res.json(ret);
    }
};

/*导出查询对象*/
module.exports = {
    // 查询用户表信息
    queryAll: function (req, res, next) {
        pool.getConnection(function(err, connection) {
            connection.query('SELECT * FROM userinfo', function(err, result) {
                if(err){
                    console.log("error: "+ err.message);
                    return err;
                }
                jsonWrite(res, result);
                connection.release();
            });
        });
    },
    // 注册响应机制
    insertNew: function(req, res, next){
        pool.getConnection(function(err, connection){
            var param = req.body;
            var insertSql = 'INSERT INTO userinfo VALUES(0,?,?,?)';
            // 检查用户名是否存在
            connection.query('SELECT UserName FROM  userinfo WHERE  UserName = "'+param.username+'"',function(err,result){
                if(err){
                    console.log("ERROR:" + err.message);
                    return err;
                };
                if(result.length === 0){
                    // 用户不存在，创建新用户
                    connection.query(insertSql,[param.username,param.password,param.email], function(err,result){
                        if(result) {
                            result = {
                                code: 200,
                                msg:'增加成功'
                            };    
                        }  
                        // 以json形式，把操作结果返回给前台页面
                        // jsonWrite(res, result);
                        return res.redirect('/login');
                    }) 
                } else {
                    // 用户已存在，返回
                    result = {
                        code: 1,
                        msg:'用户已存在'
                    }; 
                    return res.redirect('/reg');
                }
                connection.release(); 
            });
            
        })
    },
    // 登陆匹配用户
    selectUser: function(req,res,next){
        pool.getConnection(function(err,connection){
            var param = req.body;
            var selectSql = 'SELECT UserPass FROM userinfo WHERE UserName = "'+param.username+'"';
            connection.query(selectSql,function(err,result){
                if(err){
                    console.log("error: "+ err.message);
                    return err; 
                }
                if(result[0] === undefined ){
                    res.send("None");
                }else {
                    if(result[0].UserPass === param.password){
                        req.session.islogin = param.username;  //将用户存入session
                        res.locals.islogin = req.session.islogin; //将session中的用户存入全局
                        res.cookie('islogin',res.locals.islogin,{maxAge:6000}); // 设置cookie
                        res.redirect('./');
                    }else {
                        res.redirect('./login');
                    }
                }
                connection.release();
            })
        })
    },
    deleteAll:function(req,res,next){
        pool.getConnection(function(err,connection){
            connection.query('TRUNCATE TABLE userinfo',function(err,result){
                if(err){
                    console.log("error: "+ err.message);
                    return err;
                }
                res.send(result);
            })
        })
    }
    
    

}




















