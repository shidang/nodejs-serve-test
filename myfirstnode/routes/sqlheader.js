/* 
* @Author: sd
* @Date:   2016-11-02 09:11:50
* @Last Modified by:   sd
* @Last Modified time: 2016-11-02 14:48:23
*/
/* 数据库已经在phpadmin中创建*/
 //调用MySQL模块

var mysql = require('mysql');

//创建连接池
var pool = mysql.createPool({
    host : '127.0.0.1', //主机
    user : 'root',      //MySQL认证用户名
    password : '',       //MySQL认证用户密码
    port : '3306',       //端口号
    database : 'nodetest', //数据库名
})

//监听connection事件
pool.on('connection', function(connection) {  
    connection.query('SET SESSION auto_increment_increment=1'); 
});



module.exports = pool;
