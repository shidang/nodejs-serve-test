/* 
* @Author: sd
* @Date:   2016-12-30 09:39:23
* @Last Modified by:   sd
* @Last Modified time: 2016-12-30 09:55:45
*/

// 引入相关模块
var express    = require('express'),
    cheerio    = require('cheerio'),
    superagent = require('superagent');

var app = express();

// 主程序
app.get('', function(req, res, next){
    // 用 superagent 去去请求抓取 https://cnodejs.org/ 的内容
    superagent.get('https://cnodejs.org/')
        .end(function(err, sres){
            // 常规错误处理
            if(err){
                return next(err)
            }
        // sres.text 里面存储着网页的 html 内容，将它传给 cheerio.load 之后
        // 就可以得到一个实现了 jquery 接口的变量，我们习惯性地将它命名为 `$`
            var $ = cheerio.load(sres.text);
            var items = [];
            $("#topic_list .topic_title").each(function(index, el) {
                var $el = $(el);
                items.push({
                    title: $el.attr('title'),
                    href: $el.attr('href')
                });
            });
            res.send(items);
        })

})

app.listen(3000, function(){
    console.log('app is listening at port 3000');
})