/* 
* @Author: sd
* @Date:   2016-12-30 10:18:46
* @Last Modified by:   sd
* @Last Modified time: 2016-12-30 11:48:08
*/

// 加载依赖
var eventproxy = require('eventproxy'),
    superagent = require('superagent'),
    cheerio    = require('cheerio'),
    url        = require('url');

var codeUrl = 'https://cnodejs.org/';

// 主程序
// 请求页面
superagent.get(codeUrl)
    .end(function(err, res){
        // 常规错误处理
        if(err){
            return console.error(err);
        }
        // 得到值传入cheerio进行处理
        var topicUrls = [];
        var $ = cheerio.load(res.text);
        // 获取首页所有链接
        $("#topic_list .topic_title").each(function(index, el) {
            var $el = $(el);
            // $el.attr('href') 本来的样子是/topic/542acd7d5d28233425538b04
            // 使用url.resolve 自动推断完整url
            var href = url.resolve(codeUrl, $el.attr('href'));
            topicUrls.push(href);
        }); 
        // 得到 topicUrls 之后
        // 得到一个 eventproxy 的实例
        var ep = new eventproxy();

        /*命令 ep 重复监听 topicUrls.length 次（在这里也就是 40 次）`topic_html` 事件再行动ep.after('topic_html', topicUrls.length, function (topics) {topics 是个数组，包含了 40 次 ep.emit('topic_html', pair) 中的那 40 个 pair*/

        // 监听内容，url长度事件
        ep.after('topic_html', topicUrls.length, function(topics){
            topics = topics.map(function(topicPair) {
                var topicUrl  = topicPair[0];
                var topicHtml = topicPair[1];
                var $ = cheerio.load(topicHtml);
                return ({
                    title: $(".topic_full_title").text().trim(),
                    href: topicUrl,
                    comment1: $(".reply_content").eq(0).find('p').text().trim(),
                });
            });
            console.log('final: ');
            console.log(topics);
        });

        topicUrls.forEach(function(topicUrl){
            superagent.get(topicUrl)
                .end(function(err, res){
                    console.log(' fetch' + topicUrl + ' successful');
                    ep.emit('topic_html', [topicUrl, res.text]);
                })
        })

})
