/* 
* @Author: sd
* @Date:   2016-12-30 14:10:03
* @Last Modified by:   sd
* @Last Modified time: 2016-12-30 14:35:17
*/
var async = require('async');

// 并发连接数的计数器
var concurrencyCount = 0;

// 会请求信息
var fetchUrl = function(url, callback) {
    // delay值在2000内，随机整数
    var delay = parseInt((Math.random() * 10000000) % 2000, 10);
    concurrencyCount++;
    console.log("现在并发数是 ", concurrencyCount, '正在抓取的是', url, '耗时'+delay+ '毫秒' );

    setTimeout(function(){
        concurrencyCount--;
        callback(null, url + 'html contents');
    }, delay)

}

var urls = [];
for(var i = 0; i < 30; i++){
    urls.push('https://cnodejs.org/?tab=all&page='+i);
}
async.mapLimit(urls, 5, function(url, callback){
    fetchUrl(url,callback);
},function(err, result){
    console.log('final: ');
    console.log(result);
})

